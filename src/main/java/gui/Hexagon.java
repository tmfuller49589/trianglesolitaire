package gui;

public class Hexagon {
	double[] xpoints = new double[6];
	double[] ypoints = new double[6];

	public Hexagon(double side, double x, double y) {
		double r3o2 = Math.sqrt(3) / 2d;

		xpoints[0] = -r3o2;
		ypoints[0] = 0.5;
		xpoints[1] = -r3o2;
		ypoints[1] = -0.5;
		xpoints[2] = 0d;
		ypoints[2] = -1d;
		xpoints[3] = r3o2;
		ypoints[3] = -0.5;
		xpoints[4] = r3o2;
		ypoints[4] = 0.5;
		xpoints[5] = 0d;
		ypoints[5] = 1d;

		// scale to side length
		for (int i = 0; i < 6; ++i) {
			xpoints[i] = xpoints[i] * side + x;
			ypoints[i] = ypoints[i] * side + y;
		}

	}

	public double[] getXpoints() {
		return xpoints;
	}

	public void setXpoints(double[] xpoints) {
		this.xpoints = xpoints;
	}

	public double[] getYpoints() {
		return ypoints;
	}

	public void setYpoints(double[] ypoints) {
		this.ypoints = ypoints;
	}

}