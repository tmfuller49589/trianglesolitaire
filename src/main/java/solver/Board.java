package solver;

import java.util.ArrayList;
import java.util.BitSet;

public class Board {
	private BitSet bitSet;
	private int N;
	private int maxRow;
	private Board parent;

	private ArrayList<Board> children;

	public Board(Board parent) {
		this.N = parent.N;
		bitSet = (BitSet) parent.getBitSet().clone();
		maxRow = parent.getMaxRow();
		this.parent = parent;
		children = new ArrayList<Board>();
	}

	public void addChild(Board childBoard) {
		children.add(childBoard);
	}

	public Board(int N) {
		this.parent = null;
		bitSet = new BitSet(N);
		this.N = N;
		maxRow = idxToRow(N - 1);
		children = new ArrayList<Board>();
	}

	public void init(int emptyHole) {
		for (int i = 0; i < N; ++i) {
			if (i == emptyHole) {
				bitSet.clear(i);
			} else {
				bitSet.set(i, true);
			}
		}
	}

	public void dump() {
		for (int row = 0; row <= maxRow; ++row) {
			for (int spc = 0; spc < maxRow - row; ++spc) {
				System.out.print(" ");
			}
			for (int col = 0; col <= row; ++col) {
				int idx = rowColToIdx(row, col);
				if (bitSet.get(idx)) {
					System.out.print("X");
				} else {
					System.out.print("o");
				}
				System.out.print(" ");
			}
			System.out.println();
		}
	}

	public BitSet getBitSet() {
		return bitSet;
	}

	public int getN() {
		return N;
	}

	public void setN(int n) {
		N = n;
	}

	public int getMaxRow() {
		return maxRow;
	}

	public boolean checkRowCol(int row, int col) {
		if (col <= row && row <= maxRow && row >= 0 && col >= 0) {
			return true;
		}
		return false;
	}

	public static int rowColToIdx(int row, int col) {
		int idx = (row * row + row) / 2 + col;
		return idx;
	}

	public static int idxToRow(int idx) {
		int row = (int) Math.ceil((-3 + Math.sqrt(9 + 8 * idx)) / 2);
		return row;
	}

	public static int idxToCol(int idx) {
		int row = idxToRow(idx);

		int rowMin = (row * row + row) / 2;
		int col = idx - rowMin;
		return col;
	}

	public Board getParent() {
		return parent;
	}

}
