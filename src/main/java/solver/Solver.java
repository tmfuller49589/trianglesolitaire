package solver;

import java.util.ArrayList;
import java.util.Scanner;

public class Solver {

	Scanner scan = new Scanner(System.in);
	private static int depth = 0;
	private ArrayList<Board> solutions = new ArrayList<Board>();
	private ArrayList<Board> flatList = new ArrayList<Board>();
	private static int games = 0;

	public static void main(String[] args) {
		Solver solver = new Solver();
		Board board = new Board(15);
		board.init(0);
		board.dump();

		Jumper.legalJumps(board);
		solver.solve(board);

	}

	public Solver() {

	}

	public ArrayList<Board> getSolutions() {
		return solutions;
	}

	public void printStats() {
		System.out.println("Positions analyzed: " + depth);
		System.out.println("Games played: " + games);
		System.out.println("Solutions found: " + solutions.size());
		System.out.println("Solutions / number of games: " + ((double) solutions.size() / games * 100d));
	}

	public ArrayList<Board> getFlatList() {
		return flatList;
	}

	public void solve(Board board) {

		++depth;
		flatList.add(board);

		ArrayList<Integer> legalJumps = Jumper.legalJumps(board);
		if (legalJumps.size() == 0) {
			++games;
		}
		for (int i = 0; i < legalJumps.size(); i += 2) {
			int startIdx = legalJumps.get(i);
			int destIdx = legalJumps.get(i + 1);
			Board childBoard = new Board(board);
			board.addChild(childBoard);
			Jumper.jump(startIdx, destIdx, childBoard);
			// childBoard.dump();
			if (childBoard.getBitSet().cardinality() == 1) {
				System.out.println("found solution, depth is " + depth);
				// printSolution(childBoard);
				solutions.add(childBoard);
				++games;
			} else {
				solve(childBoard);
			}
		}
	}

	public static int getGames() {
		return games;
	}

	private void printSolution(Board board) {
		System.out.println("+++++++++++++++++++++++++++++++++++++");
		do {
			board.dump();
			board = board.getParent();
			System.out.println("+++++++++++++++++++++++++++++++++++++");
		} while (board != null);

	}

	public Board getSolution(int solutionIndex, int boardIndex) {
		Board board = solutions.get(solutionIndex);
		for (int i = 0; i < boardIndex; ++i) {
			board = board.getParent();
		}
		return board;
	}
}
