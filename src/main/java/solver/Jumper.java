package solver;

import java.util.ArrayList;

public class Jumper {

	public static ArrayList<Integer> legalJumps(Board board) {
		ArrayList<Integer> jumps = new ArrayList<Integer>();

		for (int row = 0; row <= board.getMaxRow(); ++row) {
			for (int col = 0; col <= row; ++col) {
				int destRow = row + 2;
				int destCol = col;
				if (board.checkRowCol(destRow, destCol) && isLegit(row, col, destRow, destCol, board)) {
					int startIdx = Board.rowColToIdx(row, col);
					int destIdx = Board.rowColToIdx(destRow, destCol);
					jumps.add(startIdx);
					jumps.add(destIdx);
				}

				destRow = row - 2;
				destCol = col;
				if (board.checkRowCol(destRow, destCol) && isLegit(row, col, destRow, destCol, board)) {
					int startIdx = Board.rowColToIdx(row, col);
					int destIdx = Board.rowColToIdx(destRow, destCol);
					jumps.add(startIdx);
					jumps.add(destIdx);
				}

				destRow = row + 2;
				destCol = col + 2;
				if (board.checkRowCol(destRow, destCol) && isLegit(row, col, destRow, destCol, board)) {
					int startIdx = Board.rowColToIdx(row, col);
					int destIdx = Board.rowColToIdx(destRow, destCol);
					jumps.add(startIdx);
					jumps.add(destIdx);
				}

				destRow = row - 2;
				destCol = col - 2;
				if (board.checkRowCol(destRow, destCol) && isLegit(row, col, destRow, destCol, board)) {
					int startIdx = Board.rowColToIdx(row, col);
					int destIdx = Board.rowColToIdx(destRow, destCol);
					jumps.add(startIdx);
					jumps.add(destIdx);
				}

				destRow = row;
				destCol = col + 2;
				if (board.checkRowCol(destRow, destCol) && isLegit(row, col, destRow, destCol, board)) {
					int startIdx = Board.rowColToIdx(row, col);
					int destIdx = Board.rowColToIdx(destRow, destCol);
					jumps.add(startIdx);
					jumps.add(destIdx);
				}

				destRow = row;
				destCol = col - 2;
				if (board.checkRowCol(destRow, destCol) && isLegit(row, col, destRow, destCol, board)) {
					int startIdx = Board.rowColToIdx(row, col);
					int destIdx = Board.rowColToIdx(destRow, destCol);
					jumps.add(startIdx);
					jumps.add(destIdx);
				}
			}
		}

		return jumps;
	}

	public static void dump(ArrayList<Integer> jumps) {
		for (int i = 0; i < jumps.size(); i += 2) {
			int startIdx = jumps.get(i);
			int destIdx = jumps.get(i + 1);
			int startRow = Board.idxToRow(startIdx);
			int startCol = Board.idxToCol(startIdx);
			int destRow = Board.idxToRow(destIdx);
			int destCol = Board.idxToCol(destIdx);

			System.out.println("from (" + startRow + ", " + startCol + ") to (" + destRow + ", " + destCol + ")");
		}

	}

	public static void jump(int startIdx, int destIdx, Board board) {
		board.getBitSet().clear(startIdx);
		board.getBitSet().set(destIdx);

		int startRow = Board.idxToRow(startIdx);
		int startCol = Board.idxToCol(startIdx);
		int destRow = Board.idxToRow(destIdx);
		int destCol = Board.idxToCol(destIdx);
		int jumpRow = (startRow + destRow) / 2;
		int jumpCol = (startCol + destCol) / 2;
		int jumpIdx = Board.rowColToIdx(jumpRow, jumpCol);
		board.getBitSet().clear(jumpIdx);

	}

	public static void jump(int startRow, int startCol, int destRow, int destCol, Board board) {
		int startIdx = Board.rowColToIdx(startRow, startCol);
		int destIdx = Board.rowColToIdx(destRow, destCol);
		board.getBitSet().clear(startIdx);
		board.getBitSet().set(destIdx);
	}

	/* 
	 * Given a starting position and destination position,
	 * determine if the jump is valid.  
	 * Return true if jump is valid, false otherwise.
	 * 
	 */
	public static boolean isLegit(int startRow, int startCol, int destRow, int destCol, Board board) {
		boolean startDestLegit = false;
		if (Math.abs(startRow - destRow) == 2 && startCol == destCol) {
			startDestLegit = true;
		} else if (startRow - destRow == 2 && startCol - destCol == 2) {
			startDestLegit = true;
		} else if (destRow - startRow == 2 && destCol - startCol == 2) {
			startDestLegit = true;
		} else if (startRow == destRow && Math.abs(startCol - destCol) == 2) {
			startDestLegit = true;
		}
		if (startDestLegit == false) {
			return false;
		}

		int jumpRow = (startRow + destRow) / 2;
		int jumpCol = (startCol + destCol) / 2;
		int jumpIdx = Board.rowColToIdx(jumpRow, jumpCol);
		int startIdx = Board.rowColToIdx(startRow, startCol);
		int destIdx = Board.rowColToIdx(destRow, destCol);

		// no peg to jump over OR
		// destination occupied OR
		// start not occupied
		if (board.getBitSet().get(jumpIdx) == false || board.getBitSet().get(destIdx) == true
				|| board.getBitSet().get(startIdx) == false) {
			return false;
		}

		// System.out.println("Jumper.isLegit(): from (" + startRow + ", " + startCol +
		// ")" + startIdx + " to (" + destRow
		// + ", " + destCol + ")" + destIdx + " is legit");

		return true;
	}
}
