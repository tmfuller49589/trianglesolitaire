package gui;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import solver.Board;
import solver.Solver;

public class TriangleSolitaireApp extends Application {
	private int sideLength = 40;
	private Solver solver;
	private Board board;
	private int N = 15;
	private Hexagon[] hexagons = new Hexagon[N];
	private Group root;

	private Stage stage;
	private Scene scene;
	private Canvas canvas;
	private GraphicsContext gc;
	static int boardIndex = 0;
	static int solutionIndex = 0;
	private int width = 600;
	private int height = 600;
	boolean showSolutions = true;

	public TriangleSolitaireApp() {
		solver = new Solver();
		board = new Board(N);
		board.init(0);
		board.dump();

		solver.solve(board);
		solver.printStats();
	}

	@Override
	public void start(Stage stageX) {
		this.stage = stageX;

		canvas = new Canvas(width, height);
		gc = canvas.getGraphicsContext2D();

		root = new Group();
		root.getChildren().add(canvas);
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

		initBoard(board);

		if (showSolutions) {
			solutionIndex = 0;
			boardIndex = 0;
			new AnimationTimer() {
				long lastUpdate = 0;

				@Override
				public void handle(long currentNanoTime) {
					if (currentNanoTime - lastUpdate > 0.3e9) {
						lastUpdate = currentNanoTime;
						Board childboard = TriangleSolitaireApp.this.solver.getSolution(solutionIndex, boardIndex);
						if (childboard.getParent() == null) {
							++solutionIndex;
							boardIndex = 0;
						} else {
							++boardIndex;
						}
						drawBoard(childboard);
					}
				}
			}.start();
		} else {
			new AnimationTimer() {
				long lastUpdate = 0;

				@Override
				public void handle(long currentNanoTime) {
					if (currentNanoTime - lastUpdate > .005e9) {
						lastUpdate = currentNanoTime;
						Board childboard = TriangleSolitaireApp.this.solver.getFlatList().get(boardIndex);
						drawBoard(childboard);
						++boardIndex;
						if (boardIndex == TriangleSolitaireApp.this.solver.getFlatList().size()) {
							boardIndex = 0;
							System.exit(0);
						}
					}
				}
			}.start();
		}
	}

	public void initBoard(Board childBoard) {
		double w = stage.getWidth();

		for (int row = 0; row <= childBoard.getMaxRow(); ++row) {
			double rowXshift = w / 2 - (childBoard.getMaxRow() - row * (double) sideLength);
			double rowYshift = sideLength * 2 + row * Math.sqrt(3) * sideLength;

			System.out.println("row = " + row + " rowXshift = " + rowXshift + " rowYshift = " + rowYshift);

			for (int col = 0; col <= row; ++col) {
				double x = rowXshift - col * 4 * (double) sideLength / 2;
				Hexagon hex = new Hexagon(sideLength, x, rowYshift);
				int idx = Board.rowColToIdx(row, col);
				hexagons[idx] = hex;
				if (childBoard.getBitSet().get(idx)) {
					gc.fillPolygon(hex.getXpoints(), hex.getYpoints(), 6);
				} else {
					gc.strokePolygon(hex.getXpoints(), hex.getYpoints(), 6);
				}
			}
		}
	}

	public void drawBoard(Board childBoard) {
		gc.clearRect(0, 0, width, height);
		for (int row = 0; row <= childBoard.getMaxRow(); ++row) {
			for (int col = 0; col <= row; ++col) {
				int idx = Board.rowColToIdx(row, col);
				Hexagon hex = hexagons[idx];
				if (childBoard.getBitSet().get(idx)) {
					gc.fillPolygon(hex.getXpoints(), hex.getYpoints(), 6);
				} else {
					gc.strokePolygon(hex.getXpoints(), hex.getYpoints(), 6);
				}
			}
		}
	}

	public static void main(String args[]) {
		launch(args);
	}
}