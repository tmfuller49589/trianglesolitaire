package solver;

import org.junit.Assert;
import org.junit.Test;

public class TriSolTester {

	@Test
	public void test() {
		int[] idxArray = new int[15];
		int[] rowArray = new int[15];
		int[] colArray = new int[15];

		for (int i = 0; i < 15; ++i) {
			idxArray[i] = i;
		}

		int idx = 0;
		for (int row = 0; row < 5; ++row) {
			for (int r = 0; r <= row; ++r) {
				rowArray[idx] = row;
				++idx;
			}
		}

		idx = 0;
		for (int row = 0; row < 5; ++row) {
			for (int r = 0; r <= row; ++r) {
				colArray[idx] = r;
				++idx;
			}
		}

		for (int i = 0; i < 15; ++i) {
			System.out.println("rowArray[" + i + "] = " + rowArray[i]);
		}

		for (int i = 0; i < 15; ++i) {
			System.out.println("colArray[" + i + "] = " + colArray[i]);
		}

		for (int i = 0; i < idxArray.length; ++i) {
			int row = Board.idxToRow(idxArray[i]);
			int col = Board.idxToCol(idxArray[i]);

			System.out.println("idx " + idxArray[i] + " is in row " + row + " col " + col);

			Assert.assertEquals("rowArray[" + i + "] = " + rowArray[i] + " row is " + row, rowArray[i], row);
			Assert.assertEquals("colArray[" + i + "] = " + colArray[i] + " col is " + col, colArray[i], col);

		}

		for (int i = 0; i < idxArray.length; ++i) {
			idx = Board.rowColToIdx(rowArray[i], colArray[i]);
			System.out.println("row " + rowArray[i] + " col " + colArray[i] + " has index " + idx);
			Assert.assertEquals("index error", idxArray[i], idx);
		}
	}
}
